﻿namespace Titles_Injector
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.label1 = new System.Windows.Forms.Label();
            this.listBoxLinks = new System.Windows.Forms.ListBox();
            this.panelNewLink = new System.Windows.Forms.Panel();
            this.buttonX = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxLink = new System.Windows.Forms.TextBox();
            this.panelAddFromClipboard = new System.Windows.Forms.Panel();
            this.buttonCloseArray = new System.Windows.Forms.Button();
            this.buttonAddArray = new System.Windows.Forms.Button();
            this.textBoxLinksArray = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.progressBarProgress = new System.Windows.Forms.ProgressBar();
            this.textBoxValues = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labelPercentComplete = new System.Windows.Forms.Label();
            this.menuStripMenuForm = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.помощьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.помощьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonSaveTo = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonAddFromClipboard = new System.Windows.Forms.Button();
            this.buttonAddNewLinkFromFile = new System.Windows.Forms.Button();
            this.buttonRemoveLink = new System.Windows.Forms.Button();
            this.buttonAddNewLink = new System.Windows.Forms.Button();
            this.checkBoxOnTop = new System.Windows.Forms.CheckBox();
            this.comboBoxEncoding = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panelNewLink.SuspendLayout();
            this.panelAddFromClipboard.SuspendLayout();
            this.menuStripMenuForm.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ссылки:";
            // 
            // listBoxLinks
            // 
            this.listBoxLinks.FormattingEnabled = true;
            this.listBoxLinks.Location = new System.Drawing.Point(12, 44);
            this.listBoxLinks.Name = "listBoxLinks";
            this.listBoxLinks.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBoxLinks.Size = new System.Drawing.Size(412, 121);
            this.listBoxLinks.TabIndex = 1;
            // 
            // panelNewLink
            // 
            this.panelNewLink.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelNewLink.Controls.Add(this.buttonX);
            this.panelNewLink.Controls.Add(this.buttonOK);
            this.panelNewLink.Controls.Add(this.label2);
            this.panelNewLink.Controls.Add(this.textBoxLink);
            this.panelNewLink.Location = new System.Drawing.Point(27, 63);
            this.panelNewLink.Name = "panelNewLink";
            this.panelNewLink.Size = new System.Drawing.Size(381, 71);
            this.panelNewLink.TabIndex = 5;
            this.panelNewLink.Visible = false;
            // 
            // buttonX
            // 
            this.buttonX.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonX.Location = new System.Drawing.Point(353, 43);
            this.buttonX.Name = "buttonX";
            this.buttonX.Size = new System.Drawing.Size(23, 23);
            this.buttonX.TabIndex = 3;
            this.buttonX.Text = "X";
            this.buttonX.UseVisualStyleBackColor = true;
            this.buttonX.Click += new System.EventHandler(this.ButtonXClick);
            // 
            // buttonOK
            // 
            this.buttonOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonOK.Location = new System.Drawing.Point(139, 43);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(99, 23);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.ButtonOkClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ссылка:";
            // 
            // textBoxLink
            // 
            this.textBoxLink.Location = new System.Drawing.Point(3, 19);
            this.textBoxLink.Name = "textBoxLink";
            this.textBoxLink.Size = new System.Drawing.Size(373, 20);
            this.textBoxLink.TabIndex = 0;
            this.textBoxLink.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxLinkKeyDown);
            // 
            // panelAddFromClipboard
            // 
            this.panelAddFromClipboard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelAddFromClipboard.Controls.Add(this.buttonCloseArray);
            this.panelAddFromClipboard.Controls.Add(this.buttonAddArray);
            this.panelAddFromClipboard.Controls.Add(this.textBoxLinksArray);
            this.panelAddFromClipboard.Controls.Add(this.label3);
            this.panelAddFromClipboard.Location = new System.Drawing.Point(13, 44);
            this.panelAddFromClipboard.Name = "panelAddFromClipboard";
            this.panelAddFromClipboard.Size = new System.Drawing.Size(411, 121);
            this.panelAddFromClipboard.TabIndex = 4;
            this.panelAddFromClipboard.Visible = false;
            // 
            // buttonCloseArray
            // 
            this.buttonCloseArray.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonCloseArray.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonCloseArray.Location = new System.Drawing.Point(380, 93);
            this.buttonCloseArray.Name = "buttonCloseArray";
            this.buttonCloseArray.Size = new System.Drawing.Size(23, 23);
            this.buttonCloseArray.TabIndex = 3;
            this.buttonCloseArray.Text = "X";
            this.buttonCloseArray.UseVisualStyleBackColor = true;
            this.buttonCloseArray.Click += new System.EventHandler(this.ButtonCloseArrayClick);
            // 
            // buttonAddArray
            // 
            this.buttonAddArray.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonAddArray.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAddArray.Location = new System.Drawing.Point(154, 93);
            this.buttonAddArray.Name = "buttonAddArray";
            this.buttonAddArray.Size = new System.Drawing.Size(99, 23);
            this.buttonAddArray.TabIndex = 2;
            this.buttonAddArray.Text = "OK";
            this.buttonAddArray.UseVisualStyleBackColor = true;
            this.buttonAddArray.Click += new System.EventHandler(this.ButtonAddArrayClick);
            // 
            // textBoxLinksArray
            // 
            this.textBoxLinksArray.Location = new System.Drawing.Point(3, 18);
            this.textBoxLinksArray.Multiline = true;
            this.textBoxLinksArray.Name = "textBoxLinksArray";
            this.textBoxLinksArray.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxLinksArray.Size = new System.Drawing.Size(400, 73);
            this.textBoxLinksArray.TabIndex = 1;
            this.textBoxLinksArray.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxLinksArrayKeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(3, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Список:";
            // 
            // progressBarProgress
            // 
            this.progressBarProgress.Location = new System.Drawing.Point(192, 171);
            this.progressBarProgress.Name = "progressBarProgress";
            this.progressBarProgress.Size = new System.Drawing.Size(256, 28);
            this.progressBarProgress.TabIndex = 6;
            // 
            // textBoxValues
            // 
            this.textBoxValues.Location = new System.Drawing.Point(15, 231);
            this.textBoxValues.Multiline = true;
            this.textBoxValues.Name = "textBoxValues";
            this.textBoxValues.ReadOnly = true;
            this.textBoxValues.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxValues.Size = new System.Drawing.Size(501, 112);
            this.textBoxValues.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "<title>:";
            // 
            // labelPercentComplete
            // 
            this.labelPercentComplete.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPercentComplete.Location = new System.Drawing.Point(454, 174);
            this.labelPercentComplete.Name = "labelPercentComplete";
            this.labelPercentComplete.Size = new System.Drawing.Size(62, 23);
            this.labelPercentComplete.TabIndex = 13;
            // 
            // menuStripMenuForm
            // 
            this.menuStripMenuForm.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.помощьToolStripMenuItem1});
            this.menuStripMenuForm.Location = new System.Drawing.Point(0, 0);
            this.menuStripMenuForm.Name = "menuStripMenuForm";
            this.menuStripMenuForm.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStripMenuForm.Size = new System.Drawing.Size(528, 24);
            this.menuStripMenuForm.TabIndex = 14;
            this.menuStripMenuForm.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "&Файл";
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.выходToolStripMenuItem.Text = "&Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.ВыходToolStripMenuItemClick);
            // 
            // помощьToolStripMenuItem1
            // 
            this.помощьToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.оПрограммеToolStripMenuItem1});
            this.помощьToolStripMenuItem1.Name = "помощьToolStripMenuItem1";
            this.помощьToolStripMenuItem1.Size = new System.Drawing.Size(68, 20);
            this.помощьToolStripMenuItem1.Text = "&Помощь";
            // 
            // оПрограммеToolStripMenuItem1
            // 
            this.оПрограммеToolStripMenuItem1.Name = "оПрограммеToolStripMenuItem1";
            this.оПрограммеToolStripMenuItem1.Size = new System.Drawing.Size(158, 22);
            this.оПрограммеToolStripMenuItem1.Text = "&О программе...";
            this.оПрограммеToolStripMenuItem1.Click += new System.EventHandler(this.ОПрограммеToolStripMenuItem1Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(48, 20);
            this.toolStripMenuItem1.Text = "&Файл";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItem3.Text = "&Выход";
            // 
            // помощьToolStripMenuItem
            // 
            this.помощьToolStripMenuItem.Name = "помощьToolStripMenuItem";
            this.помощьToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.помощьToolStripMenuItem.Text = "&Помощь";
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.оПрограммеToolStripMenuItem.Text = "&О программе...";
            // 
            // buttonSaveTo
            // 
            this.buttonSaveTo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonSaveTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSaveTo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSaveTo.Location = new System.Drawing.Point(409, 201);
            this.buttonSaveTo.Name = "buttonSaveTo";
            this.buttonSaveTo.Size = new System.Drawing.Size(107, 27);
            this.buttonSaveTo.TabIndex = 11;
            this.buttonSaveTo.Text = "Сохранить...";
            this.buttonSaveTo.UseVisualStyleBackColor = true;
            this.buttonSaveTo.Click += new System.EventHandler(this.ButtonSaveToClick);
            // 
            // buttonStop
            // 
            this.buttonStop.Enabled = false;
            this.buttonStop.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonStop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonStop.Location = new System.Drawing.Point(100, 171);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(82, 29);
            this.buttonStop.TabIndex = 8;
            this.buttonStop.Text = "Стоп";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.ButtonStopClick);
            // 
            // buttonStart
            // 
            this.buttonStart.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonStart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonStart.Location = new System.Drawing.Point(12, 171);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(82, 29);
            this.buttonStart.TabIndex = 7;
            this.buttonStart.Text = "Старт";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.ButtonStartClick);
            // 
            // buttonAddFromClipboard
            // 
            this.buttonAddFromClipboard.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonAddFromClipboard.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAddFromClipboard.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAddFromClipboard.Location = new System.Drawing.Point(430, 73);
            this.buttonAddFromClipboard.Name = "buttonAddFromClipboard";
            this.buttonAddFromClipboard.Size = new System.Drawing.Size(89, 27);
            this.buttonAddFromClipboard.TabIndex = 1;
            this.buttonAddFromClipboard.Text = "Из буфера";
            this.buttonAddFromClipboard.UseVisualStyleBackColor = true;
            this.buttonAddFromClipboard.Click += new System.EventHandler(this.ButtonAddFromClipboardClick);
            // 
            // buttonAddNewLinkFromFile
            // 
            this.buttonAddNewLinkFromFile.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonAddNewLinkFromFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAddNewLinkFromFile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAddNewLinkFromFile.Location = new System.Drawing.Point(430, 102);
            this.buttonAddNewLinkFromFile.Name = "buttonAddNewLinkFromFile";
            this.buttonAddNewLinkFromFile.Size = new System.Drawing.Size(89, 27);
            this.buttonAddNewLinkFromFile.TabIndex = 2;
            this.buttonAddNewLinkFromFile.Text = "Из файла";
            this.buttonAddNewLinkFromFile.UseVisualStyleBackColor = true;
            this.buttonAddNewLinkFromFile.Click += new System.EventHandler(this.ButtonAddNewLinkFromFileClick);
            // 
            // buttonRemoveLink
            // 
            this.buttonRemoveLink.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonRemoveLink.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonRemoveLink.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonRemoveLink.Location = new System.Drawing.Point(430, 138);
            this.buttonRemoveLink.Name = "buttonRemoveLink";
            this.buttonRemoveLink.Size = new System.Drawing.Size(89, 27);
            this.buttonRemoveLink.TabIndex = 3;
            this.buttonRemoveLink.Text = "Удалить";
            this.buttonRemoveLink.UseVisualStyleBackColor = true;
            this.buttonRemoveLink.Click += new System.EventHandler(this.ButtonRemoveLinkClick);
            // 
            // buttonAddNewLink
            // 
            this.buttonAddNewLink.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonAddNewLink.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAddNewLink.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonAddNewLink.Location = new System.Drawing.Point(430, 44);
            this.buttonAddNewLink.Name = "buttonAddNewLink";
            this.buttonAddNewLink.Size = new System.Drawing.Size(89, 27);
            this.buttonAddNewLink.TabIndex = 0;
            this.buttonAddNewLink.Text = "Добавить";
            this.buttonAddNewLink.UseVisualStyleBackColor = true;
            this.buttonAddNewLink.Click += new System.EventHandler(this.ButtonAddNewLinkClick);
            // 
            // checkBoxOnTop
            // 
            this.checkBoxOnTop.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBoxOnTop.Location = new System.Drawing.Point(430, 2);
            this.checkBoxOnTop.Name = "checkBoxOnTop";
            this.checkBoxOnTop.Size = new System.Drawing.Size(97, 20);
            this.checkBoxOnTop.TabIndex = 15;
            this.checkBoxOnTop.Text = "Всегда сверху";
            this.checkBoxOnTop.UseVisualStyleBackColor = true;
            this.checkBoxOnTop.CheckedChanged += new System.EventHandler(this.CheckBoxOnTopCheckedChanged);
            // 
            // comboBoxEncoding
            // 
            this.comboBoxEncoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEncoding.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.comboBoxEncoding.FormattingEnabled = true;
            this.comboBoxEncoding.Location = new System.Drawing.Point(287, 1);
            this.comboBoxEncoding.Name = "comboBoxEncoding";
            this.comboBoxEncoding.Size = new System.Drawing.Size(121, 21);
            this.comboBoxEncoding.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label5.Location = new System.Drawing.Point(216, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Кодировка:";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 356);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBoxEncoding);
            this.Controls.Add(this.checkBoxOnTop);
            this.Controls.Add(this.menuStripMenuForm);
            this.Controls.Add(this.buttonSaveTo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxValues);
            this.Controls.Add(this.labelPercentComplete);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.progressBarProgress);
            this.Controls.Add(this.buttonAddFromClipboard);
            this.Controls.Add(this.panelNewLink);
            this.Controls.Add(this.panelAddFromClipboard);
            this.Controls.Add(this.buttonAddNewLinkFromFile);
            this.Controls.Add(this.buttonRemoveLink);
            this.Controls.Add(this.buttonAddNewLink);
            this.Controls.Add(this.listBoxLinks);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripMenuForm;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.panelNewLink.ResumeLayout(false);
            this.panelNewLink.PerformLayout();
            this.panelAddFromClipboard.ResumeLayout(false);
            this.panelAddFromClipboard.PerformLayout();
            this.menuStripMenuForm.ResumeLayout(false);
            this.menuStripMenuForm.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBoxLinks;
        private System.Windows.Forms.Button buttonAddNewLink;
        private System.Windows.Forms.Button buttonRemoveLink;
        private System.Windows.Forms.Button buttonAddNewLinkFromFile;
        private System.Windows.Forms.Panel panelNewLink;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxLink;
        private System.Windows.Forms.Button buttonX;
        private System.Windows.Forms.Button buttonAddFromClipboard;
        private System.Windows.Forms.Panel panelAddFromClipboard;
        private System.Windows.Forms.Button buttonCloseArray;
        private System.Windows.Forms.Button buttonAddArray;
        private System.Windows.Forms.TextBox textBoxLinksArray;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar progressBarProgress;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.TextBox textBoxValues;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonSaveTo;
        private System.Windows.Forms.Label labelPercentComplete;
        private System.Windows.Forms.MenuStrip menuStripMenuForm;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem помощьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBoxOnTop;
        private System.Windows.Forms.ComboBox comboBoxEncoding;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem помощьToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem1;
    }
}

