﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Titles Injector")]
[assembly: AssemblyDescription("Mass collector for sites' title tags.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Rustam Irzaev")]
[assembly: AssemblyProduct("Titles Injector")]
[assembly: AssemblyCopyright("Copyright ©  2004-2012 Rustam Irzaev")]
[assembly: AssemblyTrademark("Rustam Irzaev")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("386944b4-b152-4d1e-8e4e-0b4481de4533")]

[assembly: AssemblyVersion("1.21.0.0")]
[assembly: AssemblyFileVersion("1.21.0.0")]
