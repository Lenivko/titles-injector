﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace Titles_Injector
{
    public partial class FormMain : Form
    {
        #region delegates
        delegate void DelegateShowProgress(int value);
        private readonly DelegateShowProgress _showProgress;

        delegate void DelegateEnableDisableControls(bool enable);
        private readonly DelegateEnableDisableControls _enableDisableControls;

        delegate void DelegateFillResults();
        private readonly DelegateFillResults _fillResults;

        delegate string DelegateGetEncoding();
        private readonly DelegateGetEncoding _getEncoding;
        #endregion

        private readonly List<string> _links;
        private readonly List<string> _values;

        private Thread _threadEvent;

        private enum AddingModeEnum
        {
            None = 0,
            Single,
            Multi,
            File
        }

        private AddingModeEnum _addingMode;

        public FormMain()
        {
            InitializeComponent();

            _links = new List<string>();
            _values = new List<string>();
            _addingMode = AddingModeEnum.None;

            _showProgress = ShowProgressVoid;
            _enableDisableControls = EnableControls;
            _fillResults = FillResultsVoid;
            _getEncoding = GetEncodingVoid;
        }

        private string GetEncodingVoid()
        {
            return comboBoxEncoding.SelectedItem.ToString();
        }

        private void ShowProgressVoid(int value)
        {
            progressBarProgress.Value = value;
            labelPercentComplete.Text = value + "%";
        }

        private void FillResultsVoid()
        {
            textBoxValues.Text = "";
            const string endEof = "\r\n";
            for (var i = 0; i < _values.Count; i++)
            {
                textBoxValues.Text += _values[i];
                if (i != _values.Count - 1)
                    textBoxValues.Text += endEof;
            }
        }

        private void EnableControls(bool enable)
        {
            foreach (Control control in Controls)
            {
                if(control.Name != "labelPercentComplete")
                    control.Enabled = enable;

                buttonStop.Enabled = !enable;

                if (_addingMode == AddingModeEnum.Single)
                    panelNewLink.Enabled = !enable;
                else if (_addingMode == AddingModeEnum.Multi)
                    panelAddFromClipboard.Enabled = !enable;
            }
        }
        
        private void FillList()
        {
            listBoxLinks.Items.Clear();

            for(var i = 0; i < _links.Count; i++)
                listBoxLinks.Items.Add(_links[i]);
        }

        private void AddSingleLink()
        {
            var link = textBoxLink.Text;
            if (link.Length > 0)
            {
                if (!_links.Contains(link))
                {
                    _links.Add(link);
                    FillList();

                    textBoxLink.Text = "";
                    textBoxLink.Focus();
                }
                else
                {
                    textBoxLink.Text = "";
                    textBoxLink.Focus();
                    MessageBox.Show(string.Format("Ссылка {0} уже имеется в списке.", link));
                }
            }
            else
            {
                MessageBox.Show("Введите ссылку.");
                textBoxLink.Focus();
            }
        }

        private void CloseSingleLink()
        {
            panelNewLink.Visible = false;
            EnableControls(true);
        }

        private void AddArray()
        {
            var lines = textBoxLinksArray.Lines;
            foreach (var line in lines)
            {
                if (line.Length > 0)
                {
                    if (!_links.Contains(line))
                    {
                        _links.Add(line);
                    }
                }
            }

            FillList();
            panelAddFromClipboard.Visible = false;
            _addingMode = AddingModeEnum.None;
            EnableControls(true);
        }

        private void CloseArray()
        {
            panelAddFromClipboard.Visible = false;
            EnableControls(true);
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            Text = ((AssemblyTitleAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false)[0]).Title;

            comboBoxEncoding.Items.AddRange(new object[] { "UTF8", "UTF7", "Unicode", "Default", "ASCII" });
            comboBoxEncoding.SelectedIndex = 0;
        }

        private void ButtonRemoveLinkClick(object sender, EventArgs e)
        {
            if (listBoxLinks.SelectedIndex != -1)
            {
                var sel = listBoxLinks.SelectedItems;
                for (var i = 0; i < sel.Count; i++)
                {
                    _links.Remove(sel[i].ToString());
                }

                FillList();
            }
        }

        private void ButtonAddNewLinkClick(object sender, EventArgs e)
        {
            _addingMode = AddingModeEnum.Single;
            EnableControls(false);
            buttonStop.Enabled = false;
            panelNewLink.Visible = true;
            textBoxLink.Text = "";
            textBoxLink.Focus();
        }

        private void ButtonXClick(object sender, EventArgs e)
        {
            _addingMode = AddingModeEnum.None;
            CloseSingleLink();
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            AddSingleLink();
        }

        private void TextBoxLinkKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                AddSingleLink();
            else if (e.KeyCode == Keys.Escape)
                CloseSingleLink();
        }

        private void ButtonAddNewLinkFromFileClick(object sender, EventArgs e)
        {
            _addingMode = AddingModeEnum.File;

            var openFileDialog = new OpenFileDialog
                      {
                          DefaultExt = ".txt",
                          Filter = @"Текстовый документ (*.txt)|*.txt|Все файлы (*.*)|*.*"
                      };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                var path = openFileDialog.FileName;
                var streamReader = new StreamReader(path);
                while (!streamReader.EndOfStream)
                {
                    var line = streamReader.ReadLine();
                    if (!_links.Contains(line) && line.Length > 0)
                    {
                        _links.Add(line);
                    }
                }
                streamReader.Close();

                FillList();
            }

            _addingMode = AddingModeEnum.None;
        }

        private void ButtonCloseArrayClick(object sender, EventArgs e)
        {
            _addingMode = AddingModeEnum.None;
            CloseArray();
        }

        private void TextBoxLinksArrayKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                CloseArray();
        }

        private void ButtonAddFromClipboardClick(object sender, EventArgs e)
        {
            _addingMode = AddingModeEnum.Multi;
            EnableControls(false);
            panelAddFromClipboard.Visible = true;
            textBoxLinksArray.Text = "";
            textBoxLinksArray.Focus();
        }

        private void ButtonAddArrayClick(object sender, EventArgs e)
        {
            AddArray();
        }

        private void ButtonStartClick(object sender, EventArgs e)
        {
            if (_links.Count > 0)
                StartEvent();
            else
            {
                MessageBox.Show("Список адресов пуст.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void StartEvent()
        {
            Invoke(_enableDisableControls, new object[] { false });

            progressBarProgress.Value = 0;
            labelPercentComplete.Text = "";
            _values.Clear();

            if (_threadEvent != null)
                _threadEvent.Abort();

            _threadEvent = null;
            _threadEvent = new Thread(EventGo);
            _threadEvent.Start();
        }

        private void StopEvent()
        {
            _threadEvent.Abort();
            _threadEvent = null;

            EnableControls(true);
            buttonStart.Enabled = true;
            buttonStop.Enabled = false;
        }

        private void EventDone()
        {
            Invoke(_enableDisableControls, new object[] { true });
            Invoke(_showProgress, new object[] { 100 });
            
            if(_threadEvent != null)
                _threadEvent.Abort();

            _threadEvent = null;
        }

        private void ButtonStopClick(object sender, EventArgs e)
        {
            StopEvent();
        }

        private void EventGo()
        {
            if (_threadEvent != null && _threadEvent.ThreadState == ThreadState.Running)
            {
                var count = _links.Count;

                for (var i = 0; i < count; i++)
                {
                    try
                    {
                        if (!_links[i].StartsWith("http://"))
                            _links[i] = "http://" + _links[i];

                        var uri = _links[i];
                        var request = (HttpWebRequest)WebRequest.Create(uri);
                        request.UseDefaultCredentials = true;
                        request.KeepAlive = false;
                        var response = (HttpWebResponse)request.GetResponse();

                        const string regex = @"(?<=<title.*>)([\s\S]*)(?=</title>)";

                        if (new List<string>(response.Headers.AllKeys).Contains("Content-Type"))
                        {
                            if (response.Headers["Content-Type"].StartsWith("text/html"))
                            {
                                var web = new WebClient {UseDefaultCredentials = true};
                                var enc = Invoke(_getEncoding, new object[] { }).ToString();

                                switch(enc)
                                {
                                    case "UTF8":
                                        web.Encoding = Encoding.UTF8;
                                        break;
                                    case "UTF7":
                                        web.Encoding = Encoding.UTF7;
                                        break;
                                    case "Unicode":
                                        web.Encoding = Encoding.Unicode;
                                        break;
                                    case "Default":
                                        web.Encoding = Encoding.Default;
                                        break;
                                    case "ASCII":
                                        web.Encoding = Encoding.ASCII;
                                        break;

                                    default:
                                        goto case "UTF8";
                                }

                                web.Headers.Add("Accept-Language", "ru-RU");
                                var page = web.DownloadString(uri);
                                web.Dispose();

                                var ex = new Regex(regex, RegexOptions.IgnoreCase);
                                var result = ex.Match(page).Value.Trim();
                                _values.Add(result);

                                var percent = 100 / count * (i + 1);
                                progressBarProgress.Invoke(_showProgress, new object[] { percent });
                                Invoke(_fillResults, new object[] { });
                            }
                        }

                        response.Close();
                        request.Abort();
                    }
                    catch (Exception)
                    {
                        EventDone();
                    }
                }

                EventDone();
            }
            else
            {
                EventDone();
            }
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_threadEvent != null) 
                _threadEvent.Abort();

            _threadEvent = null;
        }

        private void ButtonSaveToClick(object sender, EventArgs e)
        {
            if (textBoxValues.Text.Length == 0)
            {
                MessageBox.Show("Нечего сохранять.");
                return;
            }

            var saveFileDialog = new SaveFileDialog
                                 {
                                     Filter = "Текстовый документ (*.txt)|*.txt|Все файлы (*.*)|*.*",
                                     FileName =
                                         string.Format("Элементов_{0}_{1}", _values.Count,
                                                       DateTime.Now.ToShortTimeString().Replace(':', '-'))
                                 };

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                var streamWriter = new StreamWriter(saveFileDialog.FileName);
                for (var i = 0; i < _values.Count; i++)
                    streamWriter.WriteLine(_values[i]);
                streamWriter.Close();
            }
        }

        private void CheckBoxOnTopCheckedChanged(object sender, EventArgs e)
        {
            TopMost = ((CheckBox) sender).Checked;
        }

        private void ВыходToolStripMenuItemClick(object sender, EventArgs e)
        {
            Close();
        }

        private void ОПрограммеToolStripMenuItem1Click(object sender, EventArgs e)
        {
            var about = new AboutBoxMain();
            about.ShowDialog();
        }
    }
}